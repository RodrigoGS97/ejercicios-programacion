#include <stdio.h>
#include <stdlib.h>
#define VALORUSD 20.03

int main(){
	float valorConversion = VALORUSD; //Inicializamos las variables
	int entradaSwitch = 0;
	float montoDolares = 0;
	float montoPesos = 0;
	while(entradaSwitch != 3){ //Tendremos un while para no salir del programa hasta que lo indique el usuario
		printf("\nConversion de dolares a pesos mexicanos\n");
		printf("1. Conversion USD-MXN\n");  //Imprimimos el menu
		printf("2. Modificar el valor del dolar\n");
		printf("3. Salir\n");
		printf("Ingrese la opcion deseada: ");
		scanf("%d", &entradaSwitch); //Leemos la elecci�n del usuario
		switch(entradaSwitch){ //Usamos un switch para elegir entre las 4 opciones
			case 1:
				printf("\n Ingrese el monto de dolares a convertir: ");
				scanf("%f", &montoDolares); //Leemos el valor a convertir.
				montoPesos = montoDolares * valorConversion; //Convertimos el valor de dolares a pesos.
				printf("\nEl monto en pesos es %.2f", montoPesos); //Imprimimos el resultado
				break;
			case 2 :
				printf("\nIngrese el valor de 1 dolar en pesos mexicanos:");
				scanf("%f", &valorConversion); //Leemos el valor de entrada para reemplazar el valor por defecto.
				printf("\nEl valor ha sido cambiado.\n"); //Indicamos que el valor ha sido cambiado.
				break;
			case 3 :
				printf("Saliendo del programa.\n"); //Si se imprime esta opci�n es porque salimos de la ejecuci�n.
				break;
			default:
				printf("Opcion invalida, intente nuevamente.\n"); //Entramos a la opci�n por defecto si no se elegi� entre 1,2 o 3.
				//break;
		}
	}
	return 0;
}
