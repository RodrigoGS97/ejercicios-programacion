#include <stdio.h>
#include <stdlib.h>
#define MIN 1
#define MAX 50

int main(){
	int valorSuma, valorRetorno=0;
	int i;   //i es una variable que usaremos como contador
	printf("Ingrese un numero entero entre 1 y 50:"); //solicitamos el valor
	scanf("%d", &valorSuma);//leemos el valor
	if(valorSuma>=MIN && valorSuma<=MAX){//Verificamos que cumpla la condición dada
		for(i=MIN;i<=valorSuma;i++){ //el for recorre el valor i desde el 0 hasta el numero dado50 aumentando de 1 en 1
		valorRetorno = valorRetorno + i; //Hacemos la operación de suma ietartiva
		}
	}
	else{
		printf("\nEl valor ingresado debe ser un entero entre 1 y 50"); //Imprimimos el mensaje de error
		return 0; //terminamos la ejecución del programa
	}
	printf("\nEl valor de la suma es: %d", valorRetorno); //Segun los requerimientos no es necesario imprimirlo, pero 
	                                                      //lo haremos para ver el valor en la ejecución
	return valorRetorno; //devolvemos el valor final			
}
