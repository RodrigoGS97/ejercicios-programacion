#include <stdio.h>
#include <stdlib.h>

int main(){
	float ladoUno = 0;  //Inicializamos las variables
	float ladoDos = 0;
	float ladoTres = 0;
	float perimetro = 0;
	printf("Calculadora del perimetro de un triangulo escaleno\n");
	printf("Introduzca el valor en cm del primer lado: ");
	scanf("%f", &ladoUno); //Leemos el valor de la variable lado Uno
	printf("\nIntroduzca el valor en cm del segundo lado: ");
	scanf("%f", &ladoDos); //Leemos el valor de la variable lado Dos
	printf("\nIntroduzca el valor en cm del tercer lado: ");
	scanf("%f", &ladoTres); //Leemos el valor de la variable lado Uno
	perimetro = ladoUno + ladoDos + ladoTres; //Calculamos el perimetro
	printf("\nEl perimetro del triangulo es %.2f cm", perimetro); //Imprimimos el resultado
	return 0;
}
