#include <stdio.h>
#include <stdlib.h>

int main(){
	float lado = 0;  //Inicializamos las variables
	float perimetro = 0;
	printf("Calculadora del perimetro de un triangulo equilatero\n");
	printf("Introduzca el valor en cm de uno de los lados: ");
	scanf("%f", &lado); //Leemos el valor de la variable lado
	perimetro = lado * 3; //Calculamos el perimetro
	printf("\nEl perimetro del triangulo es %.2f cm", perimetro); //Imprimimos el resultado
	return 0;
}
