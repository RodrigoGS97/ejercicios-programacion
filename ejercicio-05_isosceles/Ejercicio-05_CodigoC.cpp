#include <stdio.h>
#include <stdlib.h>

int main(){
	float ladoIgual = 0;  //Inicializamos las variables
	float ladoDiferente = 0;  
	float perimetro = 0;
	printf("Calculadora del perimetro de un triangulo isoceles\n");
	printf("Introduzca el valor en cm de uno de los lados iguales: ");
	scanf("%f", &ladoIgual); //Leemos el valor de la variable lado Igual
	printf("\nIntroduzca el valor en cm del lado diferente: ");
	scanf("%f", &ladoDiferente); //Leemos el valor de la variable lado Diferente
	perimetro = (ladoIgual * 2) + ladoDiferente; //Calculamos el perimetro
	printf("\nEl perimetro del triangulo es %.2f cm", perimetro); //Imprimimos el resultado
	return 0;
}
