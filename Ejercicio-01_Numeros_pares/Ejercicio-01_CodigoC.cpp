#include <stdio.h>
#define MIN 0
#define MAX 50
#define DOS 2

int main(){
	int i;   //i es una variable que usaremos como contador
	for(i=MIN;i<=MAX;i++){ //el for recorre el valor i desde el 0 hasta el 50 aumentando de 1 en 1
		printf("%d\n",i*DOS);
	}
	return 0;			
}
